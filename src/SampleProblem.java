import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class SampleProblem {

	public static void main(String[] args) throws IOException {
		solve();
	}

	public static void solve() throws IOException {
		String[] name = { "a_example", "small", "large" };
		int phase = 0;
		System.out.println("Executing: " + name[phase]);

		
		Scanner in = new Scanner(
				new File(String.format("input/%s.in", name[phase])));
		PrintWriter pw = new PrintWriter(new FileWriter(
				new File(String.format("ouput/%s.out", name[phase]))));
		int tests = in.nextInt();
		for (int t = 1; t <= tests; t++) {
			pw.printf("Case #%d:", t);
			
		}
		pw.flush();
		pw.close();
	}
	
}
